package term

import (
	"fmt"
	"log"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/lisael/klte"
	"gitlab.com/lisael/klte/pkg/ui/command"
	"gitlab.com/lisael/klte/pkg/ui"
)

type CommandLine struct {
	*tview.InputField
}

func NewCommandLine() *CommandLine {
	return &CommandLine{
		InputField: tview.NewInputField(),
	}
}

func (c *CommandLine) SetFieldBackgroundColor(color tcell.Color) *CommandLine {
	c.InputField.SetFieldBackgroundColor(color)
	return c
}

func (c *CommandLine) SetFieldTextColor(color tcell.Color) *CommandLine {
	c.InputField.SetFieldTextColor(color)
	return c
}

func (c *CommandLine) Echo(text string) *CommandLine {
	c.SetPlaceholder(text)
	return c
}

func (c *CommandLine) SetPrompt(text string) *CommandLine {
	c.SetLabel(text)
	return c
}

func (c *CommandLine) SetDoneFunc(done func(key tcell.Key)) *CommandLine {
	c.InputField.SetDoneFunc(done)
	return c
}

func (c *CommandLine) SetText(text string) *CommandLine {
	c.InputField.SetText(text)
	return c
}

type Theme struct {
	Background tcell.Color
	Foreground tcell.Color
	Graphics   tcell.Color
	DirName    tcell.Color
	FileName   tcell.Color
	Opener     tcell.Color
}

var DefaultTheme = Theme{
	tcell.ColorBlack,
	tcell.ColorWhite,
	tcell.ColorDarkGray,
	tcell.ColorBlue,
	tcell.ColorWhite,
	tcell.ColorOrange,
}

type TermApp struct {
  *ui.BaseUI
	tree    *TreeView
	cmdline *CommandLine
	app     *tview.Application
	theme   Theme
	ctrl    klte.Controler
}

// NewTermApp returns a new term application
func NewTermApp() *TermApp {
	theme := DefaultTheme
	app := tview.NewApplication()

	cmdline := NewCommandLine().
		SetFieldBackgroundColor(theme.Background).
		SetFieldTextColor(theme.Foreground)

	t := &TermApp{
  	BaseUI: ui.NewBaseUI(),
		cmdline: cmdline,
		app:     app,
		theme:   DefaultTheme,
	}

	t.SetEchofFunc(t.Echof)
	t.SetCommandsFunc(t.Commands)

	t.Echo("Hit :h for help")
	return t
}

func (t *TermApp) SetModel(model klte.Tree){
  t.BaseUI.SetModel(model)
	tree := NewTreeView(model)
	tree.termApp = t
	grid := tview.NewGrid().
		SetRows(0, 1).
		AddItem(tree, 0, 0, 1, 1, 0, 0, true).
		AddItem(t.cmdline, 1, 0, 1, 1, 0, 0, true)
	t.app.SetRoot(grid, true)
}

func (t *TermApp) Commands() map[string]*command.Command {
  log.Println("commands...")
  return t.BaseUI.Commands()
}

func (t *TermApp) Echo(text string) {
	t.cmdline.Echo(text)
}

func (t *TermApp) Echof(tmpl string, args ...interface{}) {
	t.Echo(fmt.Sprintf(tmpl, args...))
}

func (t *TermApp) SetControler(ctrl klte.Controler) {
	t.ctrl = ctrl
}

func (t *TermApp) Run() error {
	return t.app.Run()
}
