package term

import (
	"log"
	"sort"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

  "gitlab.com/lisael/klte"
)

type RenderNode struct {
	node     klte.Node
	parent   *RenderNode
	children []*RenderNode
	last     bool
	textX    int // The x-coordinate of the first rune of the text.
}

func DirsFirstSorter(nodes []*RenderNode) []*RenderNode {
	dirs := make([]*RenderNode, 0)
	files := make([]*RenderNode, 0)
	for _, n := range nodes {
		if n.IsBranch() {
			dirs = append(dirs, n)
		} else {
			files = append(files, n)
		}
	}
	sort.Slice(dirs, func(i, j int) bool {
		return dirs[i].Name() < dirs[j].Name()
	})
	sort.Slice(files, func(i, j int) bool {
		return files[i].Name() < files[j].Name()
	})
	for _, f := range files {
		dirs = append(dirs, f)
	}
	return dirs
}

func (r *RenderNode) Sort(fn func([]*RenderNode) []*RenderNode) {
	r.children = fn(r.children)
	for _, c := range r.children {
		c.Sort(fn)
	}
}

func NewRenderNode(n klte.Node, parent *RenderNode) *RenderNode {
	r := &RenderNode{n, parent, make([]*RenderNode, 0), false, 0}
	if n.IsBranch() && n.IsOpen() {
		for _, c := range n.Children() {
			// log.Println(c.Name(), c.Visible())
			if c.Visible() {
				r.children = append(r.children, NewRenderNode(c, r))
			}
		}
	}
	return r
}

func (r *RenderNode) asSlice() []*RenderNode {
	res := make([]*RenderNode, 0)
	res = append(res, r)
	for _, c := range r.children {
		for _, cc := range c.asSlice() {
			res = append(res, cc)
		}
	}
	return res
}

func (r *RenderNode) markLast() {
	for _, c := range r.children {
		c.markLast()
	}
	if len(r.children) > 0 {
		r.children[len(r.children)-1].last = true
	}
}

func (r *RenderNode) IsBranch() bool {
	return r.node.IsBranch()
}

func (r *RenderNode) Name() string {
	return r.node.Name()
}

func (r *RenderNode) Open() error {
	return r.node.Open()
}

func (r *RenderNode) Close() error {
	return r.node.Close()
}

// Tree navigation events.
const (
	treeNone int = iota
	treeHome
	treeEnd
	treeUp
	treeDown
	treePageUp
	treePageDown
	treeParent
)

type TreeView struct {
	*tview.Box
	// the tree model
	model klte.Tree
	root  *RenderNode
	// The currently selected node or nil if no node is selected.
	currentNode *RenderNode
	// The movement to be performed during the call to Draw(), one of the
	// constants defined above.
	movement int
	// Vertical scroll offset.
	offsetY int
	// The visible nodes, top-down, as set by process().
	nodes   []*RenderNode
	termApp *TermApp
	mode    *InputMode
	changes chan klte.Node
}

func NewTreeView(model klte.Tree) *TreeView {
	tree := &TreeView{
		Box:     tview.NewBox(),
		model:   model,
		root:    nil,
		termApp: nil,
		mode:    NavMode,
		changes: model.Subscribe(),
	}
	go func() {
		for {
			log.Println("Waiting for changes")
			<-tree.changes
			until := time.After(50 * time.Millisecond)
		WaitingLoop:
			for {
				select {
				case <-tree.changes:
				case <-until:
					tree.root = nil
					tree.redraw()
					break WaitingLoop
				}
			}
		}
	}()
	return tree
}

func (t *TreeView) redraw() {
	log.Println("Schedule redraw")
	t.termApp.app.Draw()
}

// SetCurrentNode sets the currently selected node. Provide nil to clear all
// selections. Selected nodes must be visible and selectable, or else the
// selection will be changed to the top-most selectable and visible node.
//
// This function does NOT trigger the "changed" callback.
func (t *TreeView) SetCurrentNode(node *RenderNode) *TreeView {
	t.currentNode = node
	return t
}

// GetCurrentNode returns the currently selected node or nil of no node is
// currently selected.
func (t *TreeView) GetCurrentNode() *RenderNode {
	return t.currentNode
}

// GetScrollOffset returns the number of node rows that were skipped at the top
// of the tree view. Note that when the user navigates the tree view, this value
// is only updated after the tree view has been redrawn.
func (t *TreeView) GetScrollOffset() int {
	return t.offsetY
}

// GetRowCount returns the number of "visible" nodes. This includes nodes which
// fall outside the tree view's box but notably does not include the children
// of collapsed nodes. Note that this value is only up to date after the tree
// view has been drawn.
func (t *TreeView) GetRowCount() int {
	return len(t.nodes)
}

func (t *TreeView) pathIndex(path string) int {
	for idx, n := range t.nodes {
		if n.node.GetPath() == path {
			return idx
		}
	}
	return 0
}

// process builds the visible tree, populates the "nodes" slice, and processes
// pending selection actions.
func (t *TreeView) process() {
	_, _, _, height := t.GetInnerRect()

	// Determine visible nodes and their placement.
	t.nodes = nil
	selectedIndex := -1
	t.root.Sort(DirsFirstSorter)
	t.nodes = t.root.asSlice()
	if t.currentNode == nil {
		selectedIndex = 0
	} else {
		selectedIndex = t.pathIndex(t.currentNode.node.GetPath())
	}

	// Process selection. (Also trigger events if necessary.)
	if selectedIndex >= 0 {
		// Move the selection.
		newSelectedIndex := selectedIndex

		if t.currentNode != nil {
		VisibleLoop:
			for !t.currentNode.node.Visible() {
				if t.currentNode.parent != nil {
					t.currentNode = t.currentNode.parent
					if t.movement == treeParent {
						t.movement = treeNone
					}
				} else {
					break VisibleLoop
				}
			}
			newSelectedIndex = t.pathIndex(t.currentNode.node.GetPath())
		}

	MovementSwitch:
		switch t.movement {
		case treeUp:
			newSelectedIndex--
			if newSelectedIndex >= 0 {
				break MovementSwitch
			}
			newSelectedIndex = selectedIndex
		case treeDown:
			newSelectedIndex++
			if newSelectedIndex < len(t.nodes) {
				break MovementSwitch
			}
			newSelectedIndex = selectedIndex
		case treeHome:
			newSelectedIndex = 0
			break MovementSwitch
		case treeEnd:
			newSelectedIndex = len(t.nodes) - 1
			break MovementSwitch
		case treePageDown:
			if newSelectedIndex+height < len(t.nodes) {
				newSelectedIndex += height
			} else {
				newSelectedIndex = len(t.nodes) - 1
			}
			break MovementSwitch
		case treePageUp:
			if newSelectedIndex >= height {
				newSelectedIndex -= height
			} else {
				newSelectedIndex = 0
			}
			break MovementSwitch
		case treeParent:
			if t.currentNode.parent != nil {
				newSelectedIndex = t.pathIndex(t.currentNode.parent.node.GetPath())
			}
			break MovementSwitch
		}
		t.currentNode = t.nodes[newSelectedIndex]
		if newSelectedIndex != selectedIndex {
			t.movement = treeNone
		}
		selectedIndex = newSelectedIndex

		// Move selection into viewport.
		if selectedIndex-t.offsetY >= height {
			t.offsetY = selectedIndex - height + 1
		}
		if selectedIndex < t.offsetY {
			t.offsetY = selectedIndex
		}
	} else {
		if selectedIndex < 0 {
			t.currentNode = nil
		}
	}
}

type sString struct {
	str   string
	style tcell.Style
}

// Draw draws this primitive onto the screen.
func (t *TreeView) Draw(screen tcell.Screen) {
	t.Box.Draw(screen)
	if t.root == nil {
		log.Println("Rebuild render tree")
		t.root = NewRenderNode(t.model.Root(), nil)
	}

	t.process()

	// Scroll the tree.
	_, y, _, height := t.GetInnerRect()
	height--
	switch t.movement {
	case treeUp:
		t.offsetY--
	case treeDown:
		t.offsetY++
	case treeHome:
		t.offsetY = 0
	case treeEnd:
		t.offsetY = len(t.nodes)
	case treePageUp:
		t.offsetY -= height
	case treePageDown:
		t.offsetY += height
	}
	t.movement = treeNone

	// Fix invalid offsets.
	if t.offsetY >= len(t.nodes)-height {
		t.offsetY = len(t.nodes) - height
	}
	if t.offsetY < 0 {
		t.offsetY = 0
	}

	t.root.markLast()

	// Draw the tree.
	GraphicsStyle := tcell.StyleDefault.Background(t.termApp.theme.Background).Foreground(t.termApp.theme.Graphics)
	OpenerStyle := tcell.StyleDefault.Background(t.termApp.theme.Background).Foreground(t.termApp.theme.Opener)
	DirStyle := tcell.StyleDefault.Background(t.termApp.theme.Background).Foreground(t.termApp.theme.DirName)
	FileStyle := tcell.StyleDefault.Background(t.termApp.theme.Background).Foreground(t.termApp.theme.FileName)
	posY := y
	for index, r := range t.nodes {
		// Skip invisible parts.
		if posY >= y+height+1 {
			break
		}
		if index < t.offsetY {
			continue
		}

		parts := make([]sString, 0)

		// Build the graphics (bottom-top, we will have to reverse this)
		if r.parent != nil {
			if r.last {
				parts = append(parts, sString{"└", GraphicsStyle})
			} else {
				parts = append(parts, sString{"├", GraphicsStyle})
			}
			current := r.parent
			for current != nil && current.parent != nil {
				if current.last {
					parts = append(parts, sString{"  ", GraphicsStyle})
				} else {
					parts = append(parts, sString{"│ ", GraphicsStyle})
				}
				current = current.parent
			}
		}

		// reverse the graphics
		for i, j := 0, len(parts)-1; i < j; i, j = i+1, j-1 {
			parts[i], parts[j] = parts[j], parts[i]
		}

		// add the opener
		if r.node.IsBranch() && r.parent != nil {
			if r.node.IsOpen() {
				parts = append(parts, sString{"▾", OpenerStyle})
			} else {
				parts = append(parts, sString{"▸", OpenerStyle})
			}
		} else if r.parent != nil {
			parts = append(parts, sString{"─", GraphicsStyle})
		}

		// add the filename
		if r.node.IsBranch() {
			style := DirStyle
			if r == t.currentNode {
				style = style.Reverse(true)
			}
			parts = append(parts, sString{r.node.Name(), style})
			parts = append(parts, sString{"/", style})
		} else {
			style := FileStyle
			if r == t.currentNode {
				style = style.Reverse(true)
			}
			parts = append(parts, sString{r.node.Name(), style})
		}

		// draw
		x := 0
		for _, sStr := range parts {
			if sStr.style == DirStyle || sStr.style == FileStyle {
				if r.textX == 0 {
					r.textX = x
				}
			}
			for _, char := range sStr.str {
				screen.SetContent(x, posY, char, nil, sStr.style)
				x++
			}
		}
		// Advance.
		posY++
	}
}

type InputHandler func(t *TreeView, event *tcell.EventKey, setFocus func(p tview.Primitive))

type Mapping struct {
	rune_ rune
	keys  []tcell.EventKey
}

type InputMode struct {
	Name     string
	Parent   *InputMode
	Handler  InputHandler
	Mappings map[rune]*Mapping
}

func NewInputMode(name string, parent *InputMode, handler InputHandler) *InputMode {
	return &InputMode{name, parent, handler, nil}
}

func (m *InputMode) InputHandler(t *TreeView) func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		m.Handler(t, event, setFocus)
	}
}

var NavMode *InputMode = NewInputMode("Nav", nil,
	func(t *TreeView, event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		selectNode := func() {
			if t.currentNode != nil {
				if t.currentNode.IsBranch() {
					if t.currentNode.node.IsOpen() {
						t.currentNode.Close()
					} else {
						t.currentNode.Open()
					}
				} else {
					t.termApp.ctrl.OpenLeaf(t.currentNode.node)
				}
			}
		}
		closeNode := func() {
			if t.currentNode != nil {
				if t.currentNode.IsBranch() && t.currentNode.node.IsOpen() {
					t.currentNode.Close()
				} else {
					if t.currentNode.parent != nil {
						t.movement = treeParent
						t.currentNode.parent.Close()
					}
				}
			}
		}
		toggleFilter := func(name string) {
			err := t.model.ToggleFilter(name)
			if err != nil {
				log.Println(err)
			}
		}
		enterCmd := func() {
			t.termApp.cmdline.SetText("").
				Echo("").
				SetPrompt(":").
				SetDoneFunc(func(key tcell.Key) {
					log.Println("done")
					t.termApp.Execute(t.termApp.cmdline.GetText())
					t.termApp.cmdline.SetText("").SetPrompt("")
					setFocus(t)
				})
			setFocus(t.termApp.cmdline)
		}

		// Because the tree is flattened into a list only at drawing time, we also
		// postpone the (selection) movement to drawing time.
		switch key := event.Key(); key {
		case tcell.KeyTab, tcell.KeyDown:
			t.movement = treeDown
		case tcell.KeyBacktab, tcell.KeyUp:
			t.movement = treeUp
		case tcell.KeyHome:
			t.movement = treeHome
		case tcell.KeyEnd:
			t.movement = treeEnd
		case tcell.KeyPgDn, tcell.KeyCtrlF:
			t.movement = treePageDown
		case tcell.KeyPgUp, tcell.KeyCtrlB:
			t.movement = treePageUp
		case tcell.KeyRight:
			selectNode()
		case tcell.KeyLeft:
			closeNode()
		case tcell.KeyRune:
			switch event.Rune() {
			case 'q':
				t.termApp.app.Stop()
			case 'j':
				t.movement = treeDown
			case 'k':
				t.movement = treeUp
			case 'o':
				selectNode()
			case 'l':
				selectNode()
			case 'h':
				closeNode()
			case 'I':
				toggleFilter("hidden")
			case ':':
				enterCmd()
			}
		case tcell.KeyEnter:
			selectNode()
		}

		t.process()
	})

// InputHandler returns the handler for this primitive.
func (t *TreeView) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return t.WrapInputHandler(t.mode.InputHandler(t))
}

