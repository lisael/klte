package ui

import (
  "log"

  "gitlab.com/lisael/klte"
  "gitlab.com/lisael/klte/pkg/ui/command"
)

type BaseUI struct{
	commands map[string]*command.Command
	commandsFunc func () map[string]*command.Command
	echofFunc func (string, ...interface{})
	model klte.Tree
}

func NewBaseUI() *BaseUI {
  u := &BaseUI{}
 	u.commands = map[string]*command.Command{
    "echo": &command.Command{
      Name: "echo",
      Alias: "",
      Doc: "echos stuff",
      Options: map[string]command.Option{},
      Minargs: 1,
      Maxargs: -1,
      Exec: func(options map[string]interface{}, args []interface{}) error{
        u.echofFunc(args[0].(string))
        return nil
      },
    },
    "toggle-filter": &command.Command{
      Name: "toggle-filter",
      Alias: "",
      Doc: "Toggle the given filter",
      Options: map[string]command.Option{},
      Minargs: 1,
      Maxargs: 1,
      Exec: func(options map[string]interface{}, args []interface{}) error{
        return u.model.ToggleFilter(args[0].(string))
      },
    },
  }
  u.commandsFunc = u.Commands
  u.echofFunc = func (string, ...interface{}) {}
	return u
}

func (u *BaseUI) Model() klte.Tree{
  return u.model
}


func (u *BaseUI) SetModel(model klte.Tree){
  u.model = model
}

func (u *BaseUI) SetCommandsFunc(f func () map[string]*command.Command) {
  u.commandsFunc = f
}

func (u *BaseUI) SetEchofFunc(f func (string, ...interface{})){
  u.echofFunc = f
}

func (u *BaseUI) Execute(cmd string) {
  log.Println("execute..")
  if u.commandsFunc == nil {
    return
  }
  log.Println("1")
  items := command.LexCmd(cmd)
  cmdname := <-items
  command, ok := u.commandsFunc()[cmdname.Value()]
  if !ok{
    u.echofFunc("`%s`: unknown command", cmdname.Value())
    return
  }
	if err := command.Execute(items); err != nil {
		u.echofFunc("%s", err)
	}
}

func (u *BaseUI) Commands() map[string]*command.Command {
  return u.commands
}
