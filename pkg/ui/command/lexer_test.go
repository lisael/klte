package command

import (
	"fmt"
	"testing"
)

func lexString(input string) []item {
	l := lex(input)
	tokens := make([]item, 0)
	for t := range l.items {
		tokens = append(tokens, t)
	}
	return tokens
}

func checkLex(input string, expected []item) error {
	result := lexString(input)
	if len(result) != len(expected) {
		return fmt.Errorf("Expected %d tokens, got %d", len(expected), len(result))
	}
	for i := 0; i < len(expected); i++ {
		if result[i] != expected[i] {
			return fmt.Errorf("Expected token %d: %s. Got %s", i, expected[i], result[i])
		}
	}
	return nil
}

func TestLexer(t *testing.T) {
	if err := checkLex("foo", [](item){
		item{itemIdent, 0, "foo"},
		item{itemEOF, 3, ""},
	}); err != nil {
		t.Error(err)
	}
	if err := checkLex("foo bar", [](item){
		item{itemIdent, 0, "foo"},
		item{itemString, 4, "bar"},
		item{itemEOF, 7, ""},
	}); err != nil {
		t.Error(err)
	}
	if err := checkLex(`foo "bar baz"`, [](item){
		item{itemIdent, 0, "foo"},
		item{itemString, 5, "bar baz"},
		item{itemEOF, 13, ""},
	}); err != nil {
		t.Error(err)
	}
	if err := checkLex(`foo 'bar baz'`, [](item){
		item{itemIdent, 0, "foo"},
		item{itemString, 5, "bar baz"},
		item{itemEOF, 13, ""},
	}); err != nil {
		t.Error(err)
	}
}
