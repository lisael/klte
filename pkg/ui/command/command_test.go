package command

import (
	"testing"
	_"fmt"
)

var strResult = ""

var commands = map[string]*Command{
  "echo": &Command{
    Name: "echo",
    Alias: "",
    Doc: "echos stuff",
    Options: map[string]Option{},
    Minargs: 1,
    Maxargs: 3,
    Exec: func(options map[string]interface{}, args []interface{}) error{
      strResult = args[0].(string)
      return nil
    },
  },
}

func execCmd(input string) error {
	l := lex(input)
	cmditem := <- l.items
	cmdname := cmditem.val
	cmd := commands[cmdname]
	return cmd.Execute(l.items)
}


func TestCommdn(t *testing.T) {
  err := execCmd("echo foo")
  if err != nil {
		t.Error(err)
  }
  if strResult != "foo"{
    t.Error(strResult)
  }
}
