package command

import (
	"fmt"
	"strings"
	_ "unicode"
	"unicode/utf8"
)

type Pos int

func (p Pos) Position() Pos {
	return p
}

// item represents a token or text string returned from the scanner.
type item struct {
	typ itemType // The type of this item.
	pos Pos      // The starting position, in bytes, of this item in the input string.
	val string   // The value of this item.
}

func (i item) Value() string {
  return i.val
}

func (i item) String() string {
	switch {
	case i.typ == itemEOF:
		return "EOF"
	case i.typ == itemError:
		return i.val
	case len(i.val) > 10:
		return fmt.Sprintf("%.10q...(%s)", i.val, i.typ.String())
	}
	return fmt.Sprintf("%q(%q)", i.val, i.typ)
}

// itemType identifies the type of lex items.
type itemType int

const (
	itemError itemType = iota // error occurred; value is text of error
	itemIdent
	itemWhiteSpace
	itemString
	itemOpt
	itemEOF
)

func (it itemType) String() string {
	switch it {
	case itemError:
		return "ERROR"
	case itemIdent:
		return "IDENT"
	case itemWhiteSpace:
  	return "WHITESPACE"
	case itemString:
  	return "STRING"
	case itemEOF:
		return "EOF"
	default:
		panic(fmt.Errorf("Unknown type"))
	}
}

const eof = -1

// stateFn represents the state of the scanner as a function that returns the next state.
type stateFn func(*lexer) stateFn

// lexer holds the state of the scanner.
type lexer struct {
	input      string    // the string being scanned
	leftDelim  byte      // start of a string
	rightDelim byte      // expected end of a string
	pos        Pos       // current position in the input
	start      Pos       // start position of this item
	width      Pos       // width of last rune read from input
	items      chan item // channel of scanned items
	parenDepth int       // nesting depth of ( ) exprs
}

// next returns the next rune in the input.
func (l *lexer) next() rune {
	if int(l.pos) >= len(l.input) {
		l.width = 0
		return eof
	}
	r, w := utf8.DecodeRuneInString(l.input[l.pos:])
	l.width = Pos(w)
	l.pos += l.width
	return r
}

// peek returns but does not consume the next rune in the input.
func (l *lexer) peek() rune {
	r := l.next()
	l.backup()
	return r
}

// backup steps back one rune. Can only be called once per call of next.
func (l *lexer) backup() {
	l.pos -= l.width
}

// emit passes an item back to the client.
func (l *lexer) emit(t itemType) {
	l.items <- item{t, l.start, l.input[l.start:l.pos]}
	l.start = l.pos
}

// ignore skips over the pending input before this point.
func (l *lexer) ignore() {
	l.start = l.pos
}

// accept consumes the next rune if it's from the valid set.
func (l *lexer) accept(valid string) bool {
	if strings.ContainsRune(valid, l.next()) {
		return true
	}
	l.backup()
	return false
}

// acceptRun consumes a run of runes from the valid set.
func (l *lexer) acceptRun(valid string) {
	for strings.ContainsRune(valid, l.next()) {
	}
	l.backup()
}

// errorf returns an error token and terminates the scan by passing
// back a nil pointer that will be the next state, terminating l.nextItem.
func (l *lexer) errorf(format string, args ...interface{}) stateFn {
	l.items <- item{itemError, l.start, fmt.Sprintf(format, args...)}
	return nil
}

// nextItem returns the next item from the input.
// Called by the parser, not in the lexing goroutine.
func (l *lexer) nextItem() item {
	return <-l.items
}

// drain drains the output so the lexing goroutine will exit.
// Called by the parser, not in the lexing goroutine.
func (l *lexer) drain() {
	for range l.items {
	}
}

func LexCmd(input string) chan item{
  l := lex(input)
  return l.items
}

// lex creates a new scanner for the input string.
func lex(input string) *lexer {
	l := &lexer{
		input:      input,
		leftDelim:  0,
		rightDelim: 0,
		items:      make(chan item),
	}
	go l.run()
	return l
}

// run runs the state machine for the lexer.
func (l *lexer) run() {
	for state := lexCommandName; state != nil; {
		state = state(l)
	}
	close(l.items)
}

// state functions

const wordRunes = "abcdefghijklmnopqrstuvswxyzABCDEFGHIJKLMNOPQRSTUVSWXYZ1234567890"
const identRunes = "abcdefghijklmnopqrstuvswxyzABCDEFGHIJKLMNOPQRSTUVSWXYZ1234567890-"

func lexCommandName(l *lexer) stateFn {
	if scanWhiteSpace(l) {
		l.emit(itemWhiteSpace)
	}
	if !scanIdent(l) {
		return nil
	}
	l.emit(itemIdent)
	return lexToken
}

func scanIdent(l *lexer) bool {
	l.accept(wordRunes)
	l.acceptRun(identRunes)
	return l.pos != l.start
}

func scanWhiteSpace(l *lexer) bool {
	l.acceptRun(" \t")
	return l.pos != l.start
}

func scanOpt(l *lexer) bool {
	l.accept("-")
	l.accept(wordRunes)
	l.acceptRun(identRunes)
	return l.pos >= l.start+2
}

func scanString(delim rune, l *lexer) error {
	r := []rune{delim}
	if !l.accept(string(r)) {
		return fmt.Errorf("Not a string")
	}
	for {
		next := l.next()
		if next == eof {
			return fmt.Errorf("Found EOF while scanning string literal")
		}
		if next == delim {
			return nil
		}
	}
	return nil
}

func lexToken(l *lexer) stateFn {
	if scanWhiteSpace(l) {
		l.ignore()
	} else {
		next := l.peek()
		if next != eof {
			return l.errorf("Expected EOF, got %q", next)
		} else {
			l.emit(itemEOF)
			return nil
		}
	}
	peek := l.peek()
	switch {
	case peek == '-':
		if !scanOpt(l) {
			return l.errorf("Bad formated option")
		}
		l.start++
		l.emit(itemOpt)
		return lexToken
	case strings.ContainsRune("'\"`", peek):
		if err := scanString(peek, l); err != nil {
			return l.errorf(err.Error())
		}
		l.start++
		l.pos--
		l.emit(itemString)
		l.pos++
		return lexToken
	case peek == eof:
		l.emit(itemEOF)
		return nil
	case peek == '%':
		return lexBracketedString
	default:
		if scanWord(l) {
			l.emit(itemString)
			return lexToken
		}
		return l.errorf("Expetced token")
	}
	return nil
}

func lexBracketedString(l *lexer) stateFn {
	return nil
}

var wordStop = []rune{' ', '\t', eof}

func scanWord(l *lexer) bool {
MainLoop:
	for {
		next := l.next()
		for _, i := range wordStop {
			if i == next {
				break MainLoop
			}
		}
	}
	l.backup()
	return l.pos != l.start
}
