package command

import (
  "fmt"
)

type Option struct {
  Name string
  Doc string
  IsSwitch bool
  Default_ interface{}
  Types []itemType
}

type CmdExec func(options map[string]interface{}, args []interface{}) error

type Command struct{
  Name string
  Alias string
  Doc string
  Options map[string]Option
  Minargs int
  Maxargs int
  Exec CmdExec
}

type commandParser struct{
  last *item
  backuped bool
  options map[string]interface{}
  args []interface{}
  items chan item
  cmd *Command
}

func (p *commandParser) next() (*item, error){
  if p.backuped || (p.last != nil && p.last.typ == itemEOF){
    p.backuped = false
  } else {
    last := <- p.items
    if last.typ == itemError{
      return nil, fmt.Errorf(last.val)
    }
    p.last = &last
  }
  return p.last, nil
}

func (p *commandParser) backup(){
	p.backuped = true
}

func (p *commandParser) parse() error {
	if err := p.parse_options(); err != nil{
  	return err
  }
	if err := p.parse_args(); err != nil{
  	return err
  }
  return nil
}

func (p *commandParser) parse_options() error {
	for {
  	next, err := p.next()
  	if err != nil{
    	return err
  	}
  	if next.typ != itemOpt{
    	p.backup()
    	break
    }
    optname := next.val
    opt, ok := p.cmd.Options[optname]
    if !ok{
      return fmt.Errorf("Unknown option -%s", optname)
    }
    if opt.IsSwitch{
      p.options[optname] = true
      continue
    }
  	next, err = p.next()
  	if err != nil{
    	return err
  	}
    if next.typ == itemEOF || next.typ == itemOpt{
      return fmt.Errorf("Missing value for option `%s`", optname)
    }
    correctType := false
    TypeLoop:
    for _, typ := range opt.Types{
			if next.typ == typ{
  			correctType = true
  			break TypeLoop
			}
    }
    if !correctType {
      return fmt.Errorf("Wrong type for option `%s`", optname)
    }
    p.options[optname] = next.val
	}
	for k, v := range p.cmd.Options{
  	if _, ok := p.options[k]; !ok{
    	p.options[k] = v.Default_
  	}
	}
  return nil
}

func (p *commandParser) parse_args() error {
  for {
  	next, err := p.next()
  	if err != nil{
    	return err
  	}
  	if next.typ == itemEOF{
    	break
    }
  	p.args = append(p.args, next.val)
  }
  nargs := len(p.args)
  if p.cmd.Minargs >= 0 {
    if nargs < p.cmd.Minargs{
      return fmt.Errorf("Missing arguments")
    }
  }
  if p.cmd.Maxargs >= 0 {
    if nargs > p.cmd.Maxargs{
      return fmt.Errorf("Too many arguments")
    }
  }
  return nil
}


func (c *Command) Execute(tokens chan item) error {
  parser := &commandParser{
    options: make(map[string]interface{}),
   	args: make([]interface{}, 0),
   	items: tokens,
   	cmd: c,
  }
  if err := parser.parse(); err != nil{
    return err
  }
	return c.Exec(parser.options, parser.args)
}
