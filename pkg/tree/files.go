package tree

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/fsnotify/fsnotify"

	"gitlab.com/lisael/klte/internal/watcher"
	"gitlab.com/lisael/klte"
)

type NodeKind uint32

const (
	UnknownKind NodeKind = 0
	FileKind NodeKind = 1 << iota
	DirKind
	TempKind
)

// FSNode is a node of a directory tree
type FSNode struct {
	children []klte.Node
	name     string
	parent   *FSNode
	kind NodeKind
	tree *FSTree
	displayed bool
	filtered bool
	open bool
	unwatch chan struct{}
}

type FSTree struct{
	*BaseTree
	subscriptions []chan klte.Node
	watcher  *watcher.FSWatcher
}

func (t *FSTree) Subscribe() chan klte.Node {
  c := make(chan klte.Node, 100)
  t.subscriptions = append(t.subscriptions, c)
  return c
}

func (t *FSTree) Unsubscribe(c chan klte.Node) {
  n := 0
  for _, x := range t.subscriptions {
  	if x != c {
  		t.subscriptions[n] = x
  		n++
  	}
  }
  t.subscriptions = t.subscriptions[:n]
}

// ErrInvalidPath is returned when the path given to one of
// the tree operations does not exist
type ErrInvalidPath struct {
	path string
}

func (err ErrInvalidPath) Error() string {
	return fmt.Sprintf("accesing invalid path: %s", err.path)
}

func NewFSNode(name string, parent *FSNode) (*FSNode){
  var path string
  var displayed bool
  var kind NodeKind
  var children []klte.Node = nil
  var tree *FSTree
  if parent == nil {
    path = name
    displayed = true
    tree = nil
  } else {
    path = filepath.Join(parent.GetPath(), name)
    displayed = parent.displayed && parent.open
   	tree = parent.tree
  }
  filtered := false
	fi, err := os.Stat(path)
	if err != nil {
  	kind = UnknownKind
  } else {
    if fi.IsDir(){
      kind = DirKind
      children = make([]klte.Node, 0)
    } else {
      kind = FileKind
    }
  }
	node := &FSNode{children, name, parent, kind, tree, displayed, filtered, false, nil}
	return node
}

func NewDir(name string, parent *FSNode) (*FSNode, error){
	dir := NewFSNode(name, parent)
	if !dir.isDir(){
  	return nil, ErrInvalidPath{name}
  }
  if parent != nil {
    dir.tree = parent.tree
  	parent.AddChild(dir)
  }
  return dir, nil
}

func NewTree(path string) (*FSTree, error){
  root, err := NewDir(path, nil)
  if err != nil { return nil, err }
  watcher, err := watcher.NewWatcher()
  if err != nil { return nil, err }
  tree := &FSTree{
    BaseTree: NewBaseTree(root),
    subscriptions: make([]chan klte.Node, 0),
    watcher: watcher,
  }
  watcher.Run()
  root.tree = tree
  go func(){
    for{
      change := <- tree.changes
			for _, s := range tree.subscriptions{
				s<- change
			}
    }
  }()
  tree.RegisterFilter("hidden", FilterHiddenFiles)
  return tree, nil
}

func NewFile(name string, parent *FSNode) (*FSNode, error){
	file := NewFSNode(name, parent)
	if !file.IsFile(){
  	return nil, ErrInvalidPath{name}
  }
	if parent != nil {
  	parent.AddChild(file)
	}
  return file, nil
}

func (n *FSNode) isDir() bool{
  return DirKind&n.kind == DirKind
}

func (n *FSNode) IsBranch() bool{
  return n.isDir()
}

func (n *FSNode) IsFile() bool{
  return FileKind&n.kind == FileKind
}

func (n *FSNode) IsTemp() bool{
  return TempKind&n.kind == TempKind
}

func (n *FSNode) Name() string {
  return n.name
}

func FilterHiddenFiles(n klte.Node) bool{
  _, _ = n.(*FSNode)
  return strings.HasPrefix(n.Name(), ".")
}

func (n *FSNode) Children() []klte.Node {
  return n.children
}

func (n *FSNode) AddChild(child *FSNode){
  log.Printf("<%s>.AddChild(%s)\n", n.name, child.name)
  n.children = append(n.children, child)
  sort.Slice(n.children, func(i, j int) bool {
    return n.children[i].Name() < n.children[j].Name()
  })
  n.tree.ApplyFilters(child)
}

// Visible tells if a FSNode is visible (displayed and not filtered)
func (n *FSNode) Visible() bool {
	return n.displayed && !n.filtered
}

func (n *FSNode) IsOpen() bool {
  return n.open
}

// SetDisplayed recursively change the displayed property  until
// a closed dir is hit
func (tree *FSNode) SetDisplayed(displayed bool) {
  if tree.displayed == displayed { return }
  tree.displayed = displayed
 	tree.Walk(func (nn klte.Node) (bool, error){
		n := nn.(*FSNode)
    if n.displayed == displayed { return false, nil}
  	n.displayed = displayed
  	if n.kind&DirKind == DirKind{
    	if n.open {
    		if displayed{
					n.Update()
					n.Watch()
      	} else {
        	n.unwatch <- struct{}{}
      	}
      }
      return displayed && n.open, nil
    }
   	return false, nil
 	})
}

func (n *FSNode) Watch(){
 	n.unwatch = make(chan struct{})

	// listen to FS events on the open dir
 	go func(){
		events, unsub, err := n.tree.watcher.Subscribe(n.GetPath())
		if err != nil { panic(err) }
		for{
  		select{
    	case event := <-events:
        log.Printf("...%s - %s\n", event.Op, event.Name)
      	if event.Op&fsnotify.Create == fsnotify.Create{
        	relpath, err := n.RelativePath(event.Name)
        	if err != nil {
          	log.Println(err)
         	} else {
          	_, err = n.Add(relpath)
          	if err != nil { log.Println(err) }
         	}
        }
      	if event.Op&fsnotify.Remove == fsnotify.Remove{
        	log.Println("remove node...")
        	if err := n.DeleteAt(event.Name); err != nil{
          	log.Println(err)
          }
        }
      case <-n.unwatch:
        unsub()
        return
    	}
		}
  }()
}

func (n *FSNode) Basename() string{
  return n.name
}

// RelativePath returns the given path relative to n
func (n *FSNode) RelativePath(path string) (string, error){
  return filepath.Rel(n.GetPath(), path)
}

// Root recursively find the root of the node's tree
func (n *FSNode) Root() *FSNode{
  if n.parent == nil {
    return n
  }
  return n.parent.Root()
}

func (n *FSNode) Changed(){
  n.tree.changes <- n
}

func (n *FSNode) Open() error{
  if !n.isDir(){
   	return ErrInvalidPath{n.GetPath()}
  }
  if n.open { return nil }
  if err := n.Update(); err !=  nil { return err }
  for _, c := range n.children {
    c.SetDisplayed(true)
  }
  n.open = true
  n.Watch()
  n.tree.ApplyFilters(n)
  n.Changed()
  return nil
}

func (n *FSNode) Close() error{
  if !n.isDir(){
   	return ErrInvalidPath{n.GetPath()}
  }
  if !n.open { return nil }
  for _, c := range n.children {
    c.SetDisplayed(false)
  }
  n.open = false
  n.unwatch <- struct{}{}
  n.Changed()
  return nil
}

func (n *FSNode) updateDir() error{
	log.Println("Update", n.name)
	i := 0
	children := make([]klte.Node, 0)
	content, err := ioutil.ReadDir(n.GetPath())
	if err != nil { return err }
	changed := false
	for _, c := range content{
  	var old *FSNode = nil
  	if len(n.children) > i {
    	old = n.children[i].(*FSNode)
  	}
  	if old != nil && old.name == c.Name(){
    	// TODO: update FSNode info based on c FileInfo
    	children = append(children, old)
    } else {
      changed = true
      var newChild *FSNode
      var err error
      if c.IsDir(){
        newChild, err = NewDir(c.Name(), n)
      } else {
        newChild, err = NewFile(c.Name(), n)
      }
      if err != nil { return err }
      children = append(children, newChild)
    }
    if old != nil && c.Name() >= old.name {
      i++
    }
	}
	if changed{
  	// log.Println("changed")
  	n.children = children
  	n.Changed()
	}
	return nil
}

func (n *FSNode) updateFile(){return}

func (n *FSNode) Update() error{
  if n.isDir(){
    if err := n.updateDir(); err != nil {
      log.Println(err)
      return err
    }
  } else {
    n.updateFile()
  }
 	return nil
}

func (t FSNode) findFile(name string) (*FSNode, bool) {
	for _, c := range t.children {
		if c.Name() == name {
			return c.(*FSNode), true
		}
	}
	return nil, false
}

func (t *FSNode) deleteFile(name string) (result bool) {
  n := 0
	for _, i := range t.children {
		if i.Name() != name {
			t.children[n] = i
			n++
			result = true
		}
	}
	t.children = t.children[:n]
	return
}

func (n *FSNode) Walk(walker klte.Walker) error{
  if !n.isDir(){ return ErrInvalidPath{n.name} }
  for _, c := range n.Children() {
		fsc := c.(*FSNode)
    ok, err := walker(c)
    if err != nil { return err }
    if ok && fsc.isDir(){
      if err := fsc.Walk(walker); err != nil { return err }
    }
  }
  return nil
}

// GetChildren returns the directoryies/files of a directory
// determiend by path
func (t *FSNode) GetChildren(path string) ([]string, error) {
	parts := pathToParts(path)
	current := t

	for _, part := range parts {
		if child, ok := current.findFile(part); ok {
			current = child
		} else {
			return nil, ErrInvalidPath{path}
		}
	}

	keys := make([]string, 0, len(current.children))
	for _, child := range current.children {
		keys = append(keys, child.Name())
	}

	return keys, nil
}

// Add adds a directory to the directory tree
func (n *FSNode) Add(path string) (*FSNode, error) {
  log.Println("Add", n.name, path)
	parts := pathToParts(path)
	current := n

	var err error
	for _, part := range parts {
		if child, ok := current.findFile(part); ok {
			current = child
		} else {
			newPart := make([]byte, len(part))
			copy(newPart, []byte(part))
			child, err = NewFile(string(newPart), current)
			if err != nil {
  			child, err = NewDir(string(newPart), current)
  			if err != nil {
					return nil, err
    		}
  		}
			current = child
		}
		n.Changed()
	}
	return current, nil
}

// DeleteAt deletes a directory and its subdirectories/files from the tree
func (n *FSNode) DeleteAt(path string) (err error) {
  if path, err = n.RelativePath(path); err != nil{
    return err
  }
	parts := pathToParts(path)
	current := *n
	log.Printf("path... %q", parts)
	for _, part := range parts[:len(parts)-1] {
		if child, ok := current.findFile(part); ok {
			current = *child
		} else {
			return ErrInvalidPath{path}
		}
	}

	ok := current.deleteFile(parts[len(parts)-1])
	if !ok {
		return ErrInvalidPath{path}
	}
	n.Changed()
	return nil
}

func (n *FSNode) GetChild(name string) (klte.Node, error) {
  for _, c := range n.children{
    if c.Name() == name{
      return c, nil
    }
  }
  return nil, ErrInvalidPath{name}
}

func (n *FSNode) GetPath() string {
	parts := make([]string, 0, 10) // faster?
	current := n

	for ; current.parent != nil; current = current.parent {
		parts = append(parts, current.name)
	}
	parts = append(parts, current.name)

	var builder strings.Builder
	for i := len(parts) - 1; i >= 0; i-- {
  	part := parts[i]
  	if strings.HasPrefix(part, "/"){
    	part = part[1:]
    }
		builder.WriteString("/")
		builder.WriteString(part)
	}

	return builder.String()
}

func pathToParts(path string) []string {
  if strings.HasPrefix(path, "/"){
  	return strings.Split(path, "/")[1:]
  } else {
  	return strings.Split(path, "/")
  }
}

func (n *FSNode) Filtered() bool{
  return n.filtered
}

func (n *FSNode) SetFiltered(f bool){
  n.filtered = f
}

