package tree

import (
	"fmt"

	"gitlab.com/lisael/klte"
)

// Filter functions recieve a FSNode and returns wether it's filtered out
type Filter func(t klte.Node) bool

type treeFilter struct {
	filter  Filter
	enabled bool
}

type BaseTree struct {
	filters map[string]*treeFilter
	root    klte.Node
	changes chan klte.Node
}

func NewBaseTree(root klte.Node) *BaseTree {
	return &BaseTree{
		root:    root,
		filters: make(map[string]*treeFilter, 0),
		changes: make(chan klte.Node),
	}
}

func (t *BaseTree) RegisterFilter(name string, f Filter) {
	t.filters[name] = &treeFilter{f, false}
}

func (t *BaseTree) ToggleFilter(name string) error {
	f := t.filters[name]
	if f == nil {
		return fmt.Errorf("Filter `%s` not found", name)
	}
	f.enabled = !f.enabled
	t.ApplyFilters(nil)
	return nil
}

func (t *BaseTree) EnableFilter(name string) error {
	f := t.filters[name]
	if f == nil {
		return fmt.Errorf("Filter `%s` not found", name)
	}
	f.enabled = true
	t.ApplyFilters(nil)
	return nil
}

func (t *BaseTree) DisableFilter(name string) error {
	f := t.filters[name]
	if f == nil {
		return fmt.Errorf("Filter `%s` not found", name)
	}
	f.enabled = false
	t.ApplyFilters(nil)
	return nil
}

func (t *BaseTree) Root() klte.Node {
	return t.root
}

func (t *BaseTree) ApplyFilters(root klte.Node) {
	if root == nil {
		root = t.root
	}
	root.Walk(func(n klte.Node) (bool, error) {
		filtered := false
	FilterLoop:
		for _, filter := range t.filters {
			if filter.enabled {
				filtered = filter.filter(n)
				if filtered {
					break FilterLoop
				}
			}
		}
		if filtered != n.Filtered() {
			n.SetFiltered(filtered)
			t.changes <- n
		}
		if filtered {
			return false, nil
		}
		if n.IsBranch() {
			if n.IsOpen() {
				return true, nil
			} else {
				return false, nil
			}
		}
		return true, nil
	})
}
