package client

import (
  "gitlab.com/lisael/klte"
)

type Client interface{
  Echo(str string) error
  OpenLeaf(klte.Node) error
}
