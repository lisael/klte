package client

import (
  "os/exec"
  "strings"
  "fmt"
  "io"
  "log"

  "gitlab.com/lisael/klte"
)

type Kakoune struct{
  sess, client, winid string
  wmc klte.WMControler
}

func NewKakoune(sess, client, winid string, wmc klte.WMControler) *Kakoune{
  return &Kakoune{sess, client, winid, wmc}
}

func kakEscape(str string) string{
  return fmt.Sprintf("%%{%s}", str)
}

func (k *Kakoune) command(cmd... string) error {
  for i:=0; i<len(cmd); i++{
  	if strings.ContainsAny(cmd[i], ` "'`){
			cmd[i] = kakEscape(cmd[i])
  	}
  }
  cmdstr := kakEscape(strings.Join(cmd, " "))
	cmdstr = fmt.Sprintf("eval -client %s %s", k.client, cmdstr)
  log.Printf("Sending kakoune `%s`\n", cmdstr)
 	c := exec.Command("kak", "-p", k.sess)
  stdin, err := c.StdinPipe()
  if err != nil {
    log.Fatal(err)
    return err
  }

  go func() {
    defer stdin.Close()
    io.WriteString(stdin, cmdstr)
  }()

  out, err := c.CombinedOutput()
  if err != nil {
    log.Println("Error while sending kakoune cmd:", string(out))
    return err
  }
  return nil
}

func (k *Kakoune)Echo(str string) error{
  return k.command("echo", str)
}

func (k *Kakoune)OpenLeaf(n klte.Node) error{
  if err := k.command("edit", n.GetPath()); err != nil{
    return err
  }
  if err := k.wmc.FocusWindow(k.winid); err != nil{
    return err
  }
  return nil
}
