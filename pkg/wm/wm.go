package wm

import (
  "bytes"
  "fmt"
  "log"
  "os"
  "os/exec"

  i3 "github.com/i3/go-i3"

	"gitlab.com/lisael/klte"
)

type I3Controler struct{}

func (i *I3Controler) runCommandf(tmpl string, args ...interface{}) error {
	if _, err := i3.RunCommand(fmt.Sprintf(tmpl, args...)); err != nil {
		return err
	}
	return nil
}

func (i *I3Controler) FocusWindow(winid string) error{
  	return i.runCommandf("[con_id=%s] focus", winid)
}

type DumbControler struct{}

func (i *DumbControler) FocusWindow(winid string) error{
  	return nil
}

func NewControler()klte.WMControler{
	if _, ok := os.LookupEnv("SWAYSOCK"); ok{
  	initI3("sway")
  	return &I3Controler{}
  }
  if _, ok := os.LookupEnv("I3SOCK"); ok{
  	initI3("i3")
  	return &I3Controler{}
  }
  return &DumbControler{}
}

func initI3(cmd string){
  i3.SocketPathHook = func() (string, error) {
		out, err := exec.Command(cmd, "--get-socketpath").CombinedOutput()
		if err != nil {
			return "", fmt.Errorf("getting sway socketpath: %v (output: %s)", err, out)
		}
		return string(out), nil
	}
	i3.IsRunningHook = func() bool {
		out, err := exec.Command("pgrep", "-c", fmt.Sprintf("%s\\$", cmd)).CombinedOutput()
		if err != nil {
			log.Printf("sway running: %v (output: %s)", err, out)
		}
		return bytes.Compare(out, []byte("1")) == 0
	}
}

