package controler

import(
	"gitlab.com/lisael/klte/pkg/client"
	"gitlab.com/lisael/klte"
)

type History struct{
  commands []string
  index int
}

type Application struct{
	model klte.Tree
	ui klte.UI
	client client.Client
}

func NewApplication(model klte.Tree, ui klte.UI, client client.Client) *Application{
  a := &Application{
    	model: model,
    	ui: ui,
    	client: client,
  }
  a.ui.SetControler(a)
  return a
}

func (a *Application) Run() error{
  root := a.model.Root()
  root.Open()
  a.ui.SetModel(a.model)
  return a.ui.Run()
}

func (a *Application) OpenLeaf(n klte.Node){
  a.client.OpenLeaf(n)
}
