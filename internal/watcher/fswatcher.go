package watcher

// inspired by https://raw.githubusercontent.com/nathany/looper/master/watch.go

import (
	"log"
	"path/filepath"
	"sync"

	"github.com/fsnotify/fsnotify"
)


type FSWatcher struct {
	sync.RWMutex
	*fsnotify.Watcher
	subscriptions map[string][]chan fsnotify.Event
}

func NewWatcher() (*FSWatcher, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	subscriptions := make(map[string][]chan fsnotify.Event)
	fsw := &FSWatcher{Watcher: watcher, subscriptions: subscriptions}
	return fsw, nil
}

type Unsubscribe func() error

func (fsw *FSWatcher) Subscribe(path string) (chan fsnotify.Event, Unsubscribe, error) {
  path = filepath.Clean(path)
  fsw.RLock()
  if len(fsw.subscriptions[path]) == 0{
    if err := fsw.Add(path); err != nil{
      fsw.RUnlock()
      return nil, nil, err
    }
  }
  fsw.RUnlock()
  c := make(chan fsnotify.Event, 100)
  fsw.Lock()
  fsw.subscriptions[path] = append(fsw.subscriptions[path], c)
  fsw.Unlock()
  // create the unsbscibe closure
  u := func () error{
    n := 0
    fsw.Lock()
    for _, x := range fsw.subscriptions[path] {
    	if x != c {
    		fsw.subscriptions[path][n] = x
    		n++
    	}
    }
    fsw.subscriptions[path] = fsw.subscriptions[path][:n]
    fsw.Unlock()
    if len(fsw.subscriptions[path]) == 0 {
      if err := fsw.Remove(path); err != nil{
        return err
      }
    }
    return nil
  }
  return c, u, nil
}

func (fsw *FSWatcher) Run() {
  go func() {
    for {
      select {
      case event := <- fsw.Events:
        log.Printf("%s - %s\n", event.Op, event.Name)
        fsw.RLock()
        subs := fsw.subscriptions[event.Name]
        if len(subs) == 0 {
          subs = fsw.subscriptions[filepath.Dir(event.Name)]
        }
        for _, sub := range subs{
          sub <- event
        }
        fsw.RUnlock()
      }
    }
  }()
}
