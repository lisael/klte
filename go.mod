module gitlab.com/lisael/klte

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gdamore/tcell v1.3.0
	github.com/rivo/tview v0.0.0-20200204110323-ae3d8cac5e4b
	github.com/spf13/cobra v0.0.5
)
