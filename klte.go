package klte

// tree-related interfaces
type Walker func(Node) (bool, error)

type Tree interface {
	ToggleFilter(name string) error
	EnableFilter(name string) error
	DisableFilter(name string) error
	Root() Node
	Subscribe() chan Node
	Unsubscribe(chan Node)
}

type Node interface {
	Open() error
	Close() error
	Visible() bool
	Name() string
	IsBranch() bool
	IsOpen() bool
	Children() []Node
	GetPath() string
	SetDisplayed(displayed bool)
	Walk(walker Walker) error
	Filtered() bool
	SetFiltered(bool)
}

// view stuff
type UI interface {
  SetModel(Tree)
	Run() error
	SetControler(Controler)
	Echo(string)
	Echof(string, ...interface{})
}

// Controler
type Controler interface {
	OpenLeaf(Node)
}

// WMControler
type WMControler interface {
  FocusWindow(string) error
}
