package cmd

import (
	"log"
	"os"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/lisael/klte/pkg/wm"
	"gitlab.com/lisael/klte/pkg/tree"
	"gitlab.com/lisael/klte/pkg/ui/term"
	"gitlab.com/lisael/klte/pkg/controler"
	"gitlab.com/lisael/klte/pkg/client"

)

var kakstr string

var rootCmd = &cobra.Command{
	Use:   "klte",
	Short: "Golang project explorer",
	Long: `Gope is a project centric file explorer`,
	Args: cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
  	// setup logging
    f, err := os.OpenFile("/tmp/klte.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
    if err != nil {
      log.Fatal(err)
    }   
    defer f.Close()
    log.SetOutput(f)
    log.Println("Starting")
  	defer func() {
    	log.Println("Bye!")
  		if p := recover(); p != nil {
  			panic(p)
  		}
  	}()

  	var ccontroler client.Client = nil

  	// parse argumnents
   	path, _ := filepath.Abs(strings.Join(args, ""))
   	if kakstr != "" {
     	log.Println("Setup kakoune", kakstr)
     	parts := strings.Split(kakstr, ":")
     	if len(parts) != 3 {
       	fmt.Printf("Bad kakoune connection string %s. See --help\n", kakstr)
       	return
      }
     	sess := parts[0]
     	kclient := parts[1]
     	winid := parts[2]
     	log.Println(sess, kclient, winid)
     	wmc := wm.NewControler()
     	ccontroler = client.NewKakoune(sess, kclient, winid, wmc)
     	ccontroler.Echo("hello world")
    }

		// create the model
    model, err := tree.NewTree(path)
    if err != nil {panic(err)}

		// create the UI
  	tree := term.NewTermApp()

  	// Bind in an application instance
  	if err := controler.NewApplication(model, tree, ccontroler).Run(); err != nil{
    	panic(err)
  	}
	},
}

func Execute() {
  rootCmd.Flags().StringVarP(&kakstr, "kak", "k", "",
                             "Kakoune connection string (session:client:windowid)")
  if err := rootCmd.Execute(); err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}
